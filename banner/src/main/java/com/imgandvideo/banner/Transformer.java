package com.imgandvideo.banner;


import androidx.viewpager.widget.ViewPager;

import com.imgandvideo.banner.transformer.AccordionTransformer;
import com.imgandvideo.banner.transformer.BackgroundToForegroundTransformer;
import com.imgandvideo.banner.transformer.CubeInTransformer;
import com.imgandvideo.banner.transformer.CubeOutTransformer;
import com.imgandvideo.banner.transformer.DefaultTransformer;
import com.imgandvideo.banner.transformer.DepthPageTransformer;
import com.imgandvideo.banner.transformer.FlipHorizontalTransformer;
import com.imgandvideo.banner.transformer.FlipVerticalTransformer;
import com.imgandvideo.banner.transformer.ForegroundToBackgroundTransformer;
import com.imgandvideo.banner.transformer.RotateDownTransformer;
import com.imgandvideo.banner.transformer.RotateUpTransformer;
import com.imgandvideo.banner.transformer.ScaleInOutTransformer;
import com.imgandvideo.banner.transformer.StackTransformer;
import com.imgandvideo.banner.transformer.TabletTransformer;
import com.imgandvideo.banner.transformer.ZoomInTransformer;
import com.imgandvideo.banner.transformer.ZoomOutSlideTransformer;
import com.imgandvideo.banner.transformer.ZoomOutTranformer;

public class Transformer {
    public static Class<? extends ViewPager.PageTransformer> Default = DefaultTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> Accordion = AccordionTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> BackgroundToForeground = BackgroundToForegroundTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> ForegroundToBackground = ForegroundToBackgroundTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> CubeIn = CubeInTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> CubeOut = CubeOutTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> DepthPage = DepthPageTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> FlipHorizontal = FlipHorizontalTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> FlipVertical = FlipVerticalTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> RotateDown = RotateDownTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> RotateUp = RotateUpTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> ScaleInOut = ScaleInOutTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> Stack = StackTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> Tablet = TabletTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> ZoomIn = ZoomInTransformer.class;
    public static Class<? extends  ViewPager.PageTransformer> ZoomOut = ZoomOutTranformer.class;
    public static Class<? extends  ViewPager.PageTransformer> ZoomOutSlide = ZoomOutSlideTransformer.class;
}
