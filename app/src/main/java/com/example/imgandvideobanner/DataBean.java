package com.example.imgandvideobanner;
import java.util.ArrayList;
import java.util.List;

public class DataBean {
    public static List<String> getVideoBannerData() {
        List<String> list = new ArrayList<>();
        list.add("https://vd2.bdstatic.com/mda-mgh23zq41embqki7/sc/cae_h264_clips/1626578064216325138/mda-mgh23zq41embqki7.mp4");
        list.add("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F2020-06-03%2F5ed769a931db1.jpg&refer=http%3A%2F%2Fpic1.win4000.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630131649&t=3e4934bc54df5a4b729e78b94b78918a");
        list.add("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fphoto.tuchong.com%2F1336313%2Ff%2F977802912.jpg&refer=http%3A%2F%2Fphoto.tuchong.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630131649&t=dbbf4cd9213b76c6e8ec217db09904b0");
        list.add("https://vd3.bdstatic.com/mda-mgsj1772vszuhzw7/sc/cae_h264/1627393046007115573/mda-mgsj1772vszuhzw7.mp4");
        list.add("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F2019-12-23%2F5e008549e7bc1.jpg&refer=http%3A%2F%2Fpic1.win4000.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630131649&t=e3085c75d4d5843ae9524f862537c6e2");
        return list;
    }

    public static List<String> getTitleData() {
        List<String> list = new ArrayList<>();
        list.add("https://vd2.bdstatic.com/mda-mgh23zq41embqki7/sc/cae_h264_clips/1626578064216325138/mda-mgh23zq41embqki7.mp4");
        list.add("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F2020-06-03%2F5ed769a931db1.jpg&refer=http%3A%2F%2Fpic1.win4000.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630131649&t=3e4934bc54df5a4b729e78b94b78918a");
        list.add("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fphoto.tuchong.com%2F1336313%2Ff%2F977802912.jpg&refer=http%3A%2F%2Fphoto.tuchong.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630131649&t=dbbf4cd9213b76c6e8ec217db09904b0");
        list.add("https://vd3.bdstatic.com/mda-mgsj1772vszuhzw7/sc/cae_h264/1627393046007115573/mda-mgsj1772vszuhzw7.mp4");
        list.add("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic1.win4000.com%2Fwallpaper%2F2019-12-23%2F5e008549e7bc1.jpg&refer=http%3A%2F%2Fpic1.win4000.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630131649&t=e3085c75d4d5843ae9524f862537c6e2");
        return list;
    }

}
