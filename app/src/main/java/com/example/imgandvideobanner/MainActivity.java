package com.example.imgandvideobanner;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.imgandvideo.banner.Banner;
import com.imgandvideo.banner.Transformer;
import com.imgandvideo.banner.listener.OnVideoStateListener;
import com.imgandvideo.banner.loader.ImageLoader;
import com.imgandvideo.banner.loader.VideoLoader;

import cn.jzvd.Jzvd;

public class MainActivity extends AppCompatActivity {
    private Banner banner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        banner=findViewById(R.id.banner);
        //是否开启自动轮播
        banner.isAutoPlay(true);
        //设置轮播时间
        banner.setDelayTime(5000);
        //设置图片加载器
        banner.setImageLoader(new GlideImageLoader());
        //设置视频加载
        banner.setVideoLoader(new IjkVideoLoader());
        //设置图片和视频集合
        banner.setImages(DataBean.getVideoBannerData());
        banner.setBannerTitles(DataBean.getTitleData());
        //设置指示器样式
        banner.setBannerStyle(3);
        //设置动画效果
        banner.setBannerAnimation(Transformer.DepthPage);
        //banner设置方法全部调用完毕时最后调用
        banner.start();

    }
    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
    }
    @Override
    public void onBackPressed() {
        if (Jzvd.backPress()) {
            return;
        }
        super.onBackPressed();

    }


    public class GlideImageLoader extends ImageLoader {
        @Override
        public void displayView(Context context, Object path, ImageView imageView, OnVideoStateListener listener) {
            //Glide 加载图片简单用法
            Glide.with(context).load(path).into(imageView);
        }
    }


    public class IjkVideoLoader extends VideoLoader {
        @Override
        public void displayView(Context context, Object path, View view, OnVideoStateListener listener) {

            //饺子视频播放器
            MyJzvdStd jzVideo = (MyJzvdStd)view;
            jzVideo.setUp((String) path, "");
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.NONE);
            //设置视频封面图
            Glide.with(context).load("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fhiphotos.baidu.com%2Fzhidao%2Fpic%2Fitem%2Fc83d70cf3bc79f3daf0d8d02b4a1cd11728b2917.jpg&refer=http%3A%2F%2Fhiphotos.baidu.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1630132165&t=adb52f1dce17c2025e02d3c023cebfa4").apply(options).into(jzVideo.posterImageView);

            jzVideo.setOnVideoStateListener(listener);
        }
        @Override
        public View createView(Context context) {
            MyJzvdStd view = new MyJzvdStd(context);
            return view;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}